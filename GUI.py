from tkinter import PhotoImage, Tk, messagebox, N, E, S, W
from tkinter.ttk import Button, Label, LabelFrame, Style
from _tkinter import TclError

from data import Game, GameOverException

_grid_options = {"padx": 5, "pady": 5, "sticky": N + E + S + W}


class PileFrame(LabelFrame):
    pile_ui: Label
    button: Button
    parent: Tk
    first_card = None

    def __init__(self, parent, pile):
        if not self.first_card:
            self.first_card = 33
        super().__init__(parent, text=self.text)
        self.pile = pile
        self.parent = parent

        # Oberste Karte
        self.pile_ui = Label(self, image=parent.assets[self.first_card])
        self.pile_ui.grid(row=0, column=0, **_grid_options)

        # Button
        self.button = Button(self, text=self.button_text, command=self._click)
        self.button.grid(row=1, column=0, **_grid_options)

        self.parent.__class__.setup_resizableness(self)

    def refresh(self):
        self.pile_ui.config(image=self.parent.assets[self.pile.top.value])

    def _click(self):
        try:
            self._action(self.parent)
        except GameOverException:
            self.parent.game_over_screen()
        self.refresh()

    @staticmethod
    def _action(main_window):
        pass


class DrawFrame(PileFrame):
    text = "Draw-Stapel"
    button_text = "Start"
    first_card = 32


class KeepFrame(PileFrame):
    text = "Keep-Stapel"
    button_text = "Keep"

    @staticmethod
    def _action(main_window):
        if main_window.game.keep():
            main_window.refresh()
        else:
            messagebox.showerror("Fehler", "Die Karte passt nicht!")


class ThrowFrame(PileFrame):
    text = "Throw-Stapel"
    button_text = "Throw"

    @staticmethod
    def _action(main_window):
        main_window.game.throw()
        main_window.refresh()


class MainWindow(Tk):
    title = "Keep or Throw"
    assets: list
    bg = "#2e7d32"

    def __init__(self):
        super().__init__()

        self.style = Style()
        self.configure(bg=self.bg)
        self.style.configure("TLabel", background=self.bg)
        self.style.configure("TLabelframe", background=self.bg)
        self.style.configure(
            "TLabelframe.Label", background=self.bg, foreground="white"
        )

        # Laden der Kartenbilder
        self.assets = []
        for i in range(34):
            self.assets.append(PhotoImage(file=f"assets/{i:02}.png"))

        self.game = Game()

        self.draw_frame = DrawFrame(self, self.game.draw_pile)
        self.draw_frame.grid(row=0, column=0, **_grid_options)
        self.keep_frame = KeepFrame(self, self.game.keep_pile)
        self.keep_frame.grid(row=0, column=1, **_grid_options)
        self.throw_frame = ThrowFrame(self, self.game.throw_pile)
        self.throw_frame.grid(row=0, column=2, **_grid_options)
        self.setup_resizableness()

        messagebox.showinfo(
            title="Willkommen bei Keep or Throw!",
            message="Keep or Throw ist ein Kartenspiel für einen Spieler.\n"
            "Die Regeln sind wie folgt:\n"
            "Es wird immer die oberste Karte des Ziehstapeles aufgedeckt und "
            "entschieden, ob die Karte...\n"
            "...behalten (to keep), also gewertet wird,\noder\n"
            "...abgelegt (throw), also nicht gewertet wird.\n\n"
            "Jedoch darf man eine Karte nur behalten, wenn ihr Wert über dem Wert der zuletzt behaltenen Karte liegt.\n"
            "Die Kartenwerte sind:\n"
            "A > K > Q > J > 10 > 9 > 8 > 7\n"
            "wobei gilt:\n"
            "Kreuz > Pik > Herz > Karo\n\n"
            "Merke:\n"
            "Herz 7 > Karo Ass",
        )

    def setup_resizableness(self):
        (col_amount, row_amount) = self.grid_size()
        for col in range(col_amount):
            self.columnconfigure(col, weight=1)
        for row in range(row_amount):
            self.rowconfigure(col, weight=1)

    def refresh(self):
        try:
            self.keep_frame.refresh()
        except IndexError:
            pass
        try:
            self.draw_frame.refresh()
        except GameOverException:
            self.game_over_screen()

        # self.draw_pile_ui.config(image=_assets[32])

    def game_over_screen(self):
        messagebox.showinfo(
            title="Herzlichen Glückwunsch!",
            message=f"Sie haben diese Runde {len(self.game.keep_pile)} Karten gesammelt!",
        )


if __name__ == "__main__":
    gui = MainWindow()
    try:
        gui.mainloop()
    finally:
        try:
            gui.destroy()
        except TclError:
            pass
