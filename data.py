from random import shuffle
from dataclasses import dataclass
from typing import Any


@dataclass
class Card:
    value: int


class Stack:
    _list: list

    @property
    def top(self):
        return self._list[-1]

    def __init__(self):
        self._list = []

    def __len__(self):
        return len(self._list)

    def pop(self):
        return self._list.pop()

    def push(self, item: Any):
        return self._list.append(item)


class GameOverException(Exception):
    pass


class DrawPile:
    _list: list
    pop = Stack.pop

    def __init__(self):
        Stack.__init__(self)
        for i in range(32):
            self._list.append(Card(i))
        shuffle(self._list)

    def pop(self):
        try:
            return Stack.pop(self)
        except IndexError:
            raise GameOverException


    @property
    def top(self):
        try:
            return Stack.top.fget(self)
        except IndexError:
            raise GameOverException


class KeepPile:
    _list: list
    top = Stack.top
    push = Stack.push
    __init__ = Stack.__init__
    __len__ = Stack.__len__


class ThrowPile:
    _list: list
    top = Stack.top
    push = Stack.push
    __init__ = Stack.__init__


class Game:
    draw_pile: DrawPile
    keep_pile: KeepPile
    throw_pile: ThrowPile

    def __init__(self):
        self.draw_pile = DrawPile()
        self.keep_pile = KeepPile()
        self.throw_pile = ThrowPile()

    def keep(self):
        if (
            len(self.keep_pile._list) == 0
            or self.draw_pile.top.value > self.keep_pile.top.value
        ):
            self.keep_pile.push(self.draw_pile.pop())
            return True
        else:
            return False

    def throw(self):
        self.throw_pile.push(self.draw_pile.pop())
