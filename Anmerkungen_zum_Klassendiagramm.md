---
title: Erklärung des Klassendiagramms
author: Anton Pung\quad Johannes Söndgen
date: 4. April 2020
---

# Datenmodell (von unten nach oben)
- Karten werden nur als Zahlen betrachtet um Komplexität möglichst gering zu halten (Attribut `value` ist gleichzeitig Identifikator)
- Draw/Keep/Throw Pile delegieren lediglich Funktionen zur Klasse Stack, daher keine direkte Beziehung zwischen den Klassen
- Keep & Throw Pile müssen nur erweitert werden: `push()`
- Draw Pile muss nur verringert werden: `pop()`
- Game verfügt über drei Stapel und hat die beiden Funktionen keep und throw, welche dementsprechend vom Draw Pile "`pop`en" und zum entsprechenden anderen Pile "`push`en"

# Benutzeroberfläche (von oben nach unten)
- `PileFrame` als Verwaltungsklasse für die Oberfläche eines Stapels. Erweitert `tkinter.ttk.LabelFrame` und besteht visuell aus einem Button und der Karte in der Mitte. Verfügt über die Methoden `refresh()` und `_click()`, die, wie die Namen schon andeuten, die angezeigte Karte der obersten Karte des reverenzierten Stapels aus dem Datenmodell anpassen und die gewünschte Aktion für einen Buttonclick ausführen. 
- Draw/Keep/Throw Frame sind die `PileFrame`s für den jeweiligen Stapel. Dabei sind die verschiedenen Buttons zu bemerken:
  - Der Button...
    - ...des Draw-Frames: lädt die oberste Karte des `DrawPile`s: nur bei Spielstart wichtig, da danach automatisch aufgedeckt wird.
    - ...des Keep-Frames: führt die `keep()` Methode der Klasse Game aus und danach `refresh()` für Draw und Keep Frame
    - ...des Throw-Frames: führt die `throw()` Methode der Klasse Game aus und danach `refresh()` für Draw und Throw Frame
  - Diese Aktionen werden durch die in den PileFrame-Subklassen überschriebene Methode `_action()` definiert.
- `MainWindow` verfügt über `Game` und die drei `PileFrame`s und nimmt damit auch eine Art Controllerrolle ein. Zusätzlich hat es ein Attribut `style` vom Typ `Style` aus `tkinter.ttk`, dieser wird automatisch auf alle spezifizierten Elemente angewandt.
- Das Attribut `assets` referenziert die Bilder der Karten und lädt diese zu Beginn in eine Liste, wobei der Index der Bilddatei dem Wert der Karte (vgl. Datenmodell `Card`s) entspricht (+Kartenrücken mit dem Index 32, transparenter Platzhalter mit dem Index 33).
- Die Methode `refresh()` aktualisiert alle PileFrames in der Oberfläche.



